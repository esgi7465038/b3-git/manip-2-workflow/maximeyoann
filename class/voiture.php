<?php
class Voiture
{
    public $marque;
    public $modele;
    public $immatriculation;

    public function __construct($marque, $modele, $immatriculation)
    {
        $this->marque = $marque;
        $this->modele = $modele;
        $this->immatriculation = $immatriculation;
    }

    public function getNom()
    {
        return $this->marque;
    }

    public function getPrenom()
    {
        return $this->modele;
    }

    public function getDatePermis()
    {
        return $this->immatriculation;
    }
}