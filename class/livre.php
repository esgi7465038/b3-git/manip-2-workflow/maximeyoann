<?php
class Livre
{
    public $emprunteur;
    public $Personne;
    public $lecture;
    public $ecriture;
    public $nullable;
    public $dateEmprunt;

    public function __construct($emprunteur, $Personne, $lecture, $ecriture, $nullable, $dateEmprunt)
    {
        $this->emprunteur = $emprunteur;
        $this->Personne = $Personne;
        $this->lecture = $lecture;
        $this->ecriture = $ecriture;
        $this->nullable = $nullable;
        $this->dateEmprunt = $dateEmprunt;
    }
}