<?php
class Personne
{
    public $nom;
    public $prenom;
    public $datePermis;

    public function __construct($nom, $prenom, $datePermis)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->datePermis = $datePermis;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getDatePermis()
    {
        return $this->datePermis;
    }
}